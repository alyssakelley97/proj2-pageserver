"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  Disclaimer: I worked on this assignment with Anne Glickenhaus. No code was copied, but ideas were shared. 

"""
import logging 
import os
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(file_name):
  """
  This server responds only to GET requests (not PUT, POST, or UPDATE).
  Any valid GET request is answered with an ascii graphic of a cat.
  """
  log.info("--- Received request ----")
  log.info("Request was {}\n***\n".format(file_name))

  # file_name = os.path.basename(parts[1])
  file_path = os.path.join("templates", file_name) # wanting to go in the pages directory
  # file_path = os.path.join(options.DOCROOT, file_name)

  log.info("The file name is {}, and the path is {}".format(file_name, file_path))

  if ("//" in file_name) or ("~" in file_name) or (".." in file_name):
    # Part (c) If a page starts with one of the symbols(~ // ..), 
    # respond with 403 forbidden error. For example, 
    # url=localhost:5000/..name.html or /~name.html would give 403 forbidden error.
    # I am doing part c first because Prof. Ram said that the 403 error took 
    # priority over the 404 error on piazza post @53
    if ("//" in file_name):
      log.info("403 - The invlaid character // is present in the file path")
    elif ("~" in file_name):
      log.info("403 - The invlaid character ~ is present in the file path")
    elif (".." in file_name):
      log.info("403 - The invlaid character .. is present in the file path")
    
    return 403

  elif os.path.exists(file_path) is False:
    # Part (b) If name.html is not in current directory Respond with 404 (not found).
    log.info("404 - This file does not exist: {}".format(file_path))

    return 404

  else:
    # Part (a) If URL ends with name.html or name.css 
    # (i.e., if path/to/name.html is in document path (from DOCROOT)),
    # send content of name.html or name.css with proper http response.

    log.info("200 - Path {} found".format(file_path))
    return 200

  return


def main():
  return_val1 = respond("sample.html")
  print(return_val1)
  return_val2 = respond("trivia.html")
  print(return_val2)
  return_val3 = respond("trivia.com")
  print(return_val3)
  return_val4 = respond("~trivia.com")
  print(return_val4)

if __name__ == "__main__":
    main()
