from flask import Flask, render_template, abort, request
import pageserver
'''
Reference: 

For a lot of the Flask logic I used this website below:
https://flask.palletsprojects.com/en/1.1.x/quickstart/

I specifically based my render_template and error redirect using abort with the example code from that website. None of the code was
copied, but it is similar. 

'''

app = Flask(__name__)

@app.route("/<path:file_link>")

def hello(file_link):

	return_code = pageserver.respond(file_link)
	print("This is the url passed in: ", file_link)
	print("\tand this is it's resulting return code: ", return_code)

	if (return_code == 403): # ASK FOR HELP: // DOES NOT WORK
	# if (".." in file_link or "~" in file_link or "//" in file_link):
		abort(403)
		# return "do something - 403"
		# file_to_render = "403.html"

	elif (return_code == 404):
		abort(404)
		# return "do something - 404"
		# file_to_render = "404.html"

	elif (return_code == 200):
		# file_to_render = file_link
		return render_template(file_link)
		# return "do something - 200"

	else:
		return "Unknown Error"


@app.errorhandler(403)
def handle_403_error(error):
	return render_template('403.html', error=403)

@app.errorhandler(404)
def handle_404_error(error):
	return render_template('404.html', error=404)

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0')
